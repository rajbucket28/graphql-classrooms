from flask import Flask, jsonify
from flask import abort
from flask import make_response
from flask import request
import mysql.connector as mysql

app = Flask(__name__)

@app.route('/classroom/buildings/', methods=['GET'])
def get_buildings():
  db = mysql.connect(
    host="localhost",
    database="raj",
    user="raj",
    passwd="r123",
    auth_plugin='mysql_native_password'
  )
  query = "select bcode,bname from BUILDING "
  cursor = db.cursor()
  cursor.execute(query)
  records = cursor.fetchall()
  bldgs = []
  for record in records:
    bldgs.append({'bldg':record[0], 'bname':record[1]})
  result = {'buildings': bldgs}
  cursor.close()
  db.close()
  return jsonify(result)

@app.route('/classroom/rooms/<string:bldg>', methods=['GET'])
def get_rooms_in_building(bldg):
  db = mysql.connect(
    host="localhost",
    database="raj",
    user="raj",
    passwd="r123",
    auth_plugin='mysql_native_password'
  )
  query = "select rnumber from ROOM where ROOM.bcode='"+bldg+"'"
  cursor = db.cursor()
  cursor.execute(query)
  records = cursor.fetchall()
  if len(records) == 0:
    abort(404)
  rooms = []
  for record in records:
    rooms.append({'rnumber':record[0]})
  result = {'rooms': rooms}
  cursor.close()
  db.close()
  return jsonify(result)

@app.route('/classroom/rooms/<string:bldg>/<string:rnumber>', methods=['GET'])
def get_room_details(bldg,rnumber):
  db = mysql.connect(
    host="localhost",
    database="raj",
    user="raj",
    passwd="r123",
    auth_plugin='mysql_native_password'
  )
  query = "select cap,layout,type,dept,bname from ROOM,BUILDING where ROOM.bcode='"+ \
           bldg+"' and ROOM.rnumber='"+rnumber+"' and ROOM.bcode=BUILDING.bcode"
  cursor = db.cursor()
  cursor.execute(query)
  records = cursor.fetchall()
  if len(records) == 0:
    abort(404)
  result = {
    'building' : records[0][4]+" ("+bldg+")",
    'rnumber' : rnumber,
    'cap': records[0][0],
    'layout': records[0][1],
    'type': records[0][2],
    'dept': records[0][3]
  }
  cursor.close()
  db.close()
  return jsonify(result)

@app.route('/classroom/rooms/<string:bldg>/<string:rnumber>/media', methods=['GET'])
def get_room_media(bldg,rnumber):
  db = mysql.connect(
    host="localhost",
    database="raj",
    user="raj",
    passwd="r123",
    auth_plugin='mysql_native_password'
  )
  query = "select MEDIA.mcode,description from ROOM,ROOMMEDIA,MEDIA where ROOM.bcode='"+ \
           bldg+"' and ROOM.rnumber='"+rnumber+"' and ROOM.bcode=ROOMMEDIA.bcode and "+ \
          "ROOM.rnumber=ROOMMEDIA.rnumber and ROOMMEDIA.mcode=MEDIA.mcode"
  cursor = db.cursor()
  cursor.execute(query)
  records = cursor.fetchall()
  if len(records) == 0:
    abort(404)
  media = []
  for record in records:
    media.append({'mcode':record[0], 'mdescription':record[1]})
  result = {'rooms': media}
  cursor.close()
  db.close()
  return jsonify(result)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(host='localhost',debug=True)
