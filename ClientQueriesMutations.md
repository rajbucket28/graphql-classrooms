<https://bitbucket.org/rajbucket28/graphql-classrooms/>

 

python3

 

pip install mysql-connector-python  mysql-connector mysql-connector-python-rf

install just one of the above!

 

pip install graphene

pip install flask

pip install flask_graphql

pip install flask_cors

 

REST

\#\#\#\#\#\#\#\#\#\# CLIENT SIDE QUERIES \#\#\#\#\#\#\#\#\#

 

<http://localhost:5000/classroom/buildings/>

<http://localhost:5000/classroom/rooms/CLSO>

<http://localhost:5000/classroom/rooms/CLSO/106>

<http://localhost:5000/classroom/rooms/CLSO/106/media>

 

GRAPHQL

 

<http://localhost:5000/classrooms/>

 

\#\#\#\#\#\#\#\#\#\# CLIENT SIDE QUERIES/MUTATIONS \#\#\#\#\#\#\#\#\#

 

QUERIES

 

{

  buildings {

    bcode

    bname

  }

}

 

{

  media {

    mcode

    description

  }

}

 

{

  rooms (building: "CLSO") {

      rnumber

  }

}

 

{

  room (building: "CLSO", rno: "206") {

      cap

      layout

      rtype

      dept

      media {

        mcode

        description

      }

  }

}

 

{

  roommedia (building: "CLSO", rno: "206") {

  mcode

  description

  }

}

 

MUTATIONS:

 

\# insert building

mutation {

  createBuilding(bcode:"2PP", bname:"2 Park Place") {

    id

    bcode

    bname

  }

}

 

\# insert room

mutation {

  createRoom (bldg:"CLSO",rnumber:"9999",cap:44,

              layout:"Round Tables",rtype:"G",dept:"",

              media:["IWS","LAC"]) {

    ok

    bldg

    rnumber

}

}

 

 

\# update room capacity

mutation {

  updateRoomCapacity (bldg:"CLSO", rnumber:"206",cap:199) {

    ok

    bldg

}

}

 

\# delete room

mutation {

  deleteRoom(bldg:"CLSO", rnumber:"9999") {

    ok

    bldg

    rnumber

  }

}

 
