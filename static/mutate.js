$(document).ready(function() {
  var url = 'http://localhost:5000/classrooms/?query={buildings{bcode bname}}';
  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      var bldgs = response.data.buildings;
      // populate for update
      var htmlCode = "<select id='bcode1' onchange='populateRooms1()'>"+
                        "<option id='None'>Select a building</option>";
      for (var i=0; i<bldgs.length; i++)
        htmlCode += "<option value='"+bldgs[i].bcode+"'>"+bldgs[i].bname+"</option>";
      htmlCode += "</select>";
      $("#building_select1").html(htmlCode);
      // populate for delete
      var htmlCode = "<select id='bcode2' onchange='populateRooms2()'>"+
                        "<option id='None'>Select a building</option>";
      for (var i=0; i<bldgs.length; i++)
        htmlCode += "<option value='"+bldgs[i].bcode+"'>"+bldgs[i].bname+"</option>";
      htmlCode += "</select>";
      $("#building_select2").html(htmlCode);
      // populate for insert
      var htmlCode = "<select id='bcode3'>"+
                        "<option id='None'>Select a building</option>";
      for (var i=0; i<bldgs.length; i++)
        htmlCode += "<option value='"+bldgs[i].bcode+"'>"+bldgs[i].bname+"</option>";
      htmlCode += "</select>";
      $("#building_select3").html(htmlCode);
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
  var url = 'http://localhost:5000/classrooms/?query={media{mcode description}}';
  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      var media = response.data.media;
      var htmlCode = "";
      for (var i=0; i<media.length; i++)
        htmlCode += "<label><input type='checkbox' name='media[]' value='"+
                    media[i].mcode+"' />"+media[i].description+"</label><br />";
      $("#media_checkboxes").html(htmlCode);
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
});

function populateRooms1() {
  var url = 'http://localhost:5000/classrooms/?query={rooms(building:"' +
            $("#bcode1").val() + '"){rnumber}}';
  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      var rooms = response.data.rooms;
      var htmlCode = "<select id='rnumber1'>"+
                     "<option id='None'>Select a room</option>";
      for (var i=0; i<rooms.length; i++)
        htmlCode += "<option value='"+rooms[i].rnumber+"'>"+rooms[i].rnumber+"</option>";
      htmlCode += "</select>";
      $("#room_select1").html(htmlCode);
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
};

function populateRooms2() {
  var url = 'http://localhost:5000/classrooms/?query={rooms(building:"' +
            $("#bcode2").val() + '"){rnumber}}';
  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      var rooms = response.data.rooms;
      var htmlCode = "<select id='rnumber2'>"+
                     "<option id='None'>Select a room</option>";
      for (var i=0; i<rooms.length; i++)
        htmlCode += "<option value='"+rooms[i].rnumber+"'>"+rooms[i].rnumber+"</option>";
      htmlCode += "</select>";
      $("#room_select2").html(htmlCode);
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
};

function validateCap(cap) { 
  return /^([1-9]|[1-9]\d+)$/.test(cap)
}

function updateRoomCap() {
  if (!validateCap($("#newcap").val())) {
    alert("Invalid Capacity");
    return;
  }
  var url = 'http://localhost:5000/classrooms/?query=mutation{updateRoomCapacity(bldg:"' +
            $("#bcode1").val() + '",rnumber:"' +
            $("#rnumber1").val() + '",cap:' + 
            $("#newcap").val() + ')'+
            '{ok bldg rnumber cap }}';
  $.ajax({
    url: url,
    type: 'POST',
    success: function(response) {
      if (response.data.updateRoomCapacity.ok) {
        var htmlCode = "<p><b>Capacity of room "+response.data.updateRoomCapacity.bldg+
                       " "+response.data.updateRoomCapacity.rnumber+" "+
                       "has been updated to "+response.data.updateRoomCapacity.cap;
        $("#update_result").html(htmlCode);
      }
      else {
        var htmlCode = "<p><b>Capacity of room "+response.data.updateRoomCapacity.bldg+
                       " "+response.data.updateRoomCapacity.rnumber+" "+
                       "could not be updated";
        $("#update_result").html(htmlCode);
      }
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
}

function deleteRoom() {
  var url = 'http://localhost:5000/classrooms/?query=mutation{deleteRoom(bldg:"' +
            $("#bcode2").val() + '",rnumber:"' +
            $("#rnumber2").val() + '"){ok bldg rnumber}}';
  console.log(url);
  $.ajax({
    url: url,
    type: 'POST',
    success: function(response) {
      if (response.data.deleteRoom.ok) {
        var htmlCode = "<p><b>Room "+response.data.deleteRoom.bldg+
                       " "+response.data.deleteRoom.rnumber+" "+
                       "has been deleted";
        $("#delete_result").html(htmlCode);
      }
      else {
        var htmlCode = "<p><b>Room "+response.data.deleteRoom.bldg+
                       " "+response.data.deleteRoom.rnumber+" "+
                       "could not be deleted";
        $("#delete_result").html(htmlCode);
      }
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
}

function insertRoom() {
  if (!validateCap($("#insert_cap").val())) {
    alert("Invalid Capacity");
    return;
  }
  if ($("#insert_rnumber").val().trim() == "") {
    alert("Room number required");
    return;
  }
  var query = 'mutation { createRoom (bldg:"'+$("#bcode3").val();
  query += '",rnumber:"'+$("#insert_rnumber").val();
  query += '",cap:'+$("#insert_cap").val();
  query += ',layout:"'+$("#insert_layout").val();
  query += '",rtype:"'+$("#insert_rtype").val();
  query += '",dept:"'+$("#insert_dept").val();
  query += '",media:[';
  var media = [];
  $("input[name='media[]']:checked").each(function () {
    media.push($(this).val());
  });
  for (var i=0; i<media.length; i++)
    if (i == media.length-1)
      query += '"'+media[i]+'"';
    else
      query += '"'+media[i]+'",';
  query += ']){ok bldg rnumber}}';
  //alert(query);
  var url = 'http://localhost:5000/classrooms/?query='+query;
  //console.log(url);
  $.ajax({
    url: url,
    type: 'POST',
    success: function(response) {
      if (response.data.createRoom.ok) {
        var htmlCode = "<p><b>Room "+response.data.createRoom.bldg+
                       " "+response.data.createRoom.rnumber+" "+
                       "has been inserted";
        $("#insert_result").html(htmlCode);
      }
      else {
        var htmlCode = "<p><b>Room "+response.data.createRoom.bldg+
                       " "+response.data.createRoom.rnumber+" "+
                       "could not be inserted";
        $("#insert_result").html(htmlCode);
      }
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
}
