$(document).ready(function() {
  var url = 'http://localhost:5000/classrooms/?query={buildings{bcode bname}}';
  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      var bldgs = response.data.buildings;
      var htmlCode = "<select id='bcode' onchange='populateRooms()'>"+
                        "<option id='None'>Select a building</option>";
      for (var i=0; i<bldgs.length; i++)
        htmlCode += "<option value='"+bldgs[i].bcode+"'>"+bldgs[i].bname+"</option>";
      htmlCode += "</select>";
      $("#building_select").html(htmlCode);
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
});

function populateRooms() {
  var url = 'http://localhost:5000/classrooms/?query={rooms(building:"' +
            $("#bcode").val() + '"){rnumber}}';
  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      var rooms = response.data.rooms;
      var htmlCode = "<select id='rnumber' onchange='getRoomDetails()' >"+
                     "<option id='None'>Select a room</option>";
      for (var i=0; i<rooms.length; i++)
        htmlCode += "<option value='"+rooms[i].rnumber+"'>"+rooms[i].rnumber+"</option>";
      htmlCode += "</select>";
      $("#room_select").html(htmlCode);
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
};

function getRoomDetails() {
  var url = 'http://localhost:5000/classrooms/?query={room(building:"' +
            $("#bcode").val() + '",rno:"' +
            $("#rnumber").val() + '")'+
            '{bldg rnumber cap layout rtype dept media {mcode description}}}';
  //console.log(url);
  $.ajax({
    url: url,
    type: 'GET',
    success: function(response) {
      var htmlCode = "<table>";
      htmlCode += "<tr><td><b>BUILDING:</b></td><td>"+response.data.room.bldg+"</td></tr>";
      htmlCode += "<tr><td><b>ROOM:</b></td><td>"+response.data.room.rnumber+"</td></tr>";
      htmlCode += "<tr><td><b>CAPACITY:</b></td><td>"+response.data.room.cap+"</td></tr>";
      htmlCode += "<tr><td><b>LAYOUT:</b></td><td>"+response.data.room.layout+"</td></tr>";
      if (response.data.room.rtype == 'P')
        htmlCode += "<tr><td><b>TYPE:</b></td><td>"+
                    response.data.room.rtype+" ("+response.data.room.dept+")</td></tr>";
      else
        htmlCode += "<tr><td><b>TYPE:</b></td><td>"+response.data.room.rtype+"</td></tr>";
      //htmlCode += "<tr><td><b>DEPT:</b></td><td>"+response.data.room.dept+"</td></tr>";
      htmlCode += "<tr><td valign='top'><b>MEDIA:</b></td><td>";
      var media = response.data.room.media;
      for (var i=0; i<media.length; i++)
        htmlCode += media[i].description+" ("+media[i].mcode+")<br />";
      htmlCode += "</td></tr></table>";
      $("#room_details").html(htmlCode);
    },
    error: function(error) {
      alert("ERROR");
      console.log(error);
    }
  });
}
